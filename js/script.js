//  Code for my-script
// pintar el documento cuando este listo.
$(document).ready(function () {
    // Cargo las recetas provenientes de la base de datos.


    loadRecipes();

    // Inicio del codigo para ver los detalles de las recetas..
    $(document).on("click", ".btn-primary", function () {
        let userId = $(this).attr("data-val-id");
        $('#containerHome').html('<section><div class="row p-3" id="cards"></div></section>');
        $.ajax({
            url: `http://stackoverflowcgvm.apphb.com/api/Recipes?Id=${userId}`,
            type: 'GET',
            success: function (data) {

                $(data).each(function (index, item) {
                    $('#cards').append(`
                    <article class="col-12 p-4  text-center bg-light" id="article-users">
                    
                    <div class="card mb-3"><span style="position:absolute; right: 0; padding: .3em; cursor: pointer" id="cerrar" onclick="closeCard();"><img
                    src="img/icon/cerrar.png" alt="iconos de me Gusta" title="Cerrar Ventana"></span><span style="position:absolute; right: 3em; padding: .3em; cursor: pointer"></span><div class="card-header bg-warning text-white"><h1>Detalles de la receta</h1></div><div class="card-body "><div class="card img-recipes d-inline-block tb-4" style="width: 39%;"><img src="${data.ImageUrl}" alt="Card image cap" class="card-img-top imagen w-100"></div><div class="card d-inline-block  align-top" style="width: 60%;"><h1 class="pt-2 pb-2 card-title text-center font-weight-bold">${data.Name}</h1><div class="p-4 bg-primary w-100 text-justify text-white ">${data.Description}</div><p class="card-text d-flex align-items-center bd-highlight mb-1 p-2 w-100"><span><img class="w-25" src="img/icon/Servings.png"> ${data.Servings} servings</span><span class="ml-auto p-2 bd-highlight"><img class="w-25" src="img/icon/time.png"> ${data.AmountOfTime} min.</span><span><img src="img/icon/author.png" style="max-width: 40%;">${data.Author}</span></p></div></div></div>
                    
                    <div class="card mb-3"><div class="table-responsive"><div class="card-header bg-warning text-white"><h1 class="text-center text-uppercase">Ingredientes</h1></div>
                    <table id="tabla" class="table"><thead><tr><th>ID</th><th>Ingredientes</th> <th>Cantidad | Descripción</th></tr></thead><tbody id="tbody">`);


                    for (i = 0; i < data.RecipeIngredientDetails.length; i++) {
                        $('#tbody').append(`<tr><td>${data.RecipeIngredientDetails[i].Id}</td><td>${data.RecipeIngredientDetails[i].IngredientName}</td><td>${data.RecipeIngredientDetails[i].QuantityDescription}</td></tr>`);
                    }

                    $('#cards').append(`<article class="col-12 p-4  text-center bg-light" id="article-users">
                    
                    <div class="card mb-3">
       
                    
                    <div class="card-header bg-warning text-white w-100">
                    <h1 class="text-center text-uppercase">Pasos a Seguir</h1></div>

                    <div class="card-body ">
                    <textarea class="form-control  font-weight-light disabled" rows="15">${data.Steps}</textarea>
                    </div></article>`);
                });
            },
            error: function (err) {
                console.error(`Error al obtener los datos ${err}`);
            }

        });

    });

});

// Inicio en función para cargar las listas en el index principal
function loadRecipes() {
    //debugger
    if (islogin) {
        $.ajax({
            url: `http://stackoverflowcgvm.apphb.com/api/Recipes`,
            type: 'GET',
            success: function (data) {
                $(data).each(function (index, data) {
                    $('#cards').append(`<article class=" col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 pt-2 gallery"> <div class="card img-recipes h-100"> <img class="card-img-top imagen h-50" src="${data.ImageUrl}" alt="posible imagen de cabecera de receta"> <div class="card-body"> <h6 class="card-title text-center font-weight-bold">${data.Name}</h6><button class="btn btn-primary w-100" data-val-id="${data.Id}">Ver detalles</button> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <span> <img class="w-25" src="img/icon/Servings.png"> ${data.Servings} servings</span> <span class="ml-auto p-2 bd-highlight"> <img class="w-25" src="img/icon/time.png"> ${data.AmountOfTime} min. </span></p> </div> <div class="card-footer bg-dark"> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <img src="img/icon/heart.png" class="mr-2 like-recipes" title="Darle me gusta" id="btn-like" data-val-id-likes="${data.Id}"><span class="text-white h2 m-0">${data.Votes} </span> <small class="text-muted ml-auto p-0 mb-0  bd-highlight h5">By ${data.Author}</small> </p> </div> </div> </article>`);
                });
            },
            error: function (err) {
                console.error(`Error al obtener los datos ${err}`);
            }

        });
    } else {
        $.ajax({
            url: `http://stackoverflowcgvm.apphb.com/api/Recipes`,
            type: 'GET',
            success: function (data) {
                $(data).each(function (index, data) {
                    $('#cards').append(`<article class=" col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 pt-2 gallery"> <div class="card img-recipes h-100"> <img class="card-img-top imagen h-50" src="${data.ImageUrl}" alt="posible imagen de cabecera de receta"> <div class="card-body"> <h6 class="card-title text-center font-weight-bold">${data.Name}</h6><button class="btn btn-primary w-100" data-val-id="${data.Id}">Ver detalles</button> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <span> <img class="w-25" src="img/icon/Servings.png"> ${data.Servings} servings</span> <span class="ml-auto p-2 bd-highlight"> <img class="w-25" src="img/icon/time.png"> ${data.AmountOfTime} min. </span></p> </div> <div class="card-footer bg-dark"> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <img src="img/icon/heart.png" class="mr-2 like-recipes" title="Loguearse" id="btn-like" data-val-id-likes="0"><span class="text-white h2 m-0">${data.Votes} </span> <small class="text-muted ml-auto p-0 mb-0  bd-highlight h5">By ${data.Author}</small> </p> </div> </div> </article>`);
                });
            },
            error: function (err) {
                console.error(`Error al obtener los datos ${err}`);
            }

        });
    }

}
// Fin en función para cargar las listas en el index principal

// 
// Inicio del codigo registrar usuario en la API.
$(document).on("click", "#btn-register", function register() {
    var htmlErroAlert = '<div class="alert alert-warning" role="alert">Es importante que empiece con !, Mayusculas, minusculas, números</div>';
    var registerData = {};
    let username = $('#regname').val();
    let email = $('#regemail').val();
    let password = $('#regpass').val();
    let ConfirmPassword = $('#reregpass').val();
    if (ConfirmPassword != password) {
        $("#errors").addClass("bg-danger");
        $("#errors").html(`<h4 class="ml-2  text-light">PASS NO COINCIDEN</h4>`);
        $('#regpass').val("").focus();
        $('#reregpass').val("");
        $('#errorAlert').html(htmlErroAlert);
        $('#errorAlert').removeClass('d-none');
    } else {
        registerData = {
            Email: email,
            Password: password,
            ConfirmPassword: ConfirmPassword,
            UserName: username
        };
        // Realiza la llamada ajaxiada.

        $.ajax({
            url: 'http://stackoverflowcgvm.apphb.com/api/Account/Register',
            type: 'POST',
            data: registerData, // Aqui se envia el objeto. por medio de data que toma los datos del formulario.
            success: function (data) {
                alert(`Datos ingresados sactifactoriamente, inicia sesión con tu cuenta`)
                document.location.href = "login.html";
            },
            error: function (error) {
                // debugger
                console.log(error.responseJSON.Message);
                $("#errors").addClass("bg-danger");
                $("#errors").html(`<h4 class="ml-2  text-light">DATOS INCORRECTOS</h4>`);

                // Limpiar las cajas de texto
                $('#regname').val("").focus();
                $('#regemail').val("");
                $('#regpass').val("");
                $('#reregpass').val("");
            }
        });
    }


});

// vuelvo a colocar el texto de register
$(document).on("change", "#regname", function register() {
    $("#errors").removeClass("bg-danger");
    $("#errors").html(`REGISTER`);
});

// vuelvo a quitar el texto del error.
$(document).on("change", "#regpass", function deleteErrorAlert() {
    $("#errorAlert").addClass("d-none");
    $("#errors").removeClass("bg-danger");
    $("#errors").html(`REGISTER`);
});

// Fin del codigo registrar usuario en la API.


// inicio del codig js para que el usuario pueda loguearse con su cuenta ya regsitrada.
$(document).on("click", "#btn-login", function loginUsers() {
    let username = $("#name").val();
    let password = $("#pass").val();
    let loginData = {
        username: username,
        password: password,
        grant_type: "password"
    };
    // le envio los datos a la API para obtener el token de acceso.
    $.ajax({
        url: 'http://stackoverflowcgvm.apphb.com/Token',
        data: loginData,
        type: 'POST',
        success: function (data) {
            localStorage.setItem("token", data.access_token);
            document.location.href = "../index.html";
        },
        error: function (error) {
            $("#errLogin").addClass("bg-danger");
            $("#errLogin").html(`<h4 class="ml-2  text-light">DATOS INVÁLIDOS</h4>`);
            console.log(error.responseJSON.error_description);
            // Limpiar las cajas de texto
            $('#name').val("").focus();
            $('#pass').val("");
        },
    })
});

// Limpiar el textbox name 
$(document).on("change", "#name", function register() {
    $("#errLogin").removeClass("bg-danger");
    $("#errLogin").html(`LOGIN`);
});

// Inicio de codigo que me permitira realizar una busqueda de las recetas de la API.

$("#search").keypress(function (e) {
    if (e.which == 13) {

        $('#cards').html('');
        var searchUser = $('#search').val();
        if (islogin) {
            $.ajax({
                url: `http://stackoverflowcgvm.apphb.com/api/Recipes?searchTerm=${searchUser}`,
                type: 'GET',
                success: function (data) {
                    $(data).each(function (index, data) {
                        $('#cards').append(`<article class=" col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 pt-2"> <div class="card img-recipes h-100"> <img class="card-img-top imagen" src="${data.ImageUrl}" alt="Card image cap"> <div class="card-body"> <h6 class="card-title text-center font-weight-bold">${data.Name}</h6><button class="btn btn-primary w-100" data-val-id="${data.Id}">Ver detalles</button> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <span> <img class="w-25" src="img/icon/Servings.png"> ${data.Servings} servings</span> <span class="ml-auto p-2 bd-highlight"> <img class="w-25" src="img/icon/time.png"> ${data.AmountOfTime} min. </span></p> </div> <div class="card-footer bg-dark"> <p class="card-text d-flex align-items-center bd-highlight mb-1">  <img src="img/icon/heart.png" class="mr-2 like-recipes" title="Darle me gusta" id="btn-like" data-val-id-likes="${data.Id}"><span class="text-white h2 m-0">${data.Votes} </span> <small class="text-muted ml-auto p-0 mb-0  bd-highlight h5">By ${data.Author}</small> </p> </div> </div> </article>`);
                    });
                },
                error: function (err) {
                    console.error(`Error al obtener los datos ${err}`);
                }

            });
        } else {
            $.ajax({
                url: `http://stackoverflowcgvm.apphb.com/api/Recipes?searchTerm=${searchUser}`,
                type: 'GET',
                success: function (data) {
                    $(data).each(function (index, data) {
                        $('#cards').append(`<article class=" col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 pt-2"> <div class="card img-recipes h-100"> <img class="card-img-top imagen" src="${data.ImageUrl}" alt="Card image cap"> <div class="card-body"> <h6 class="card-title text-center font-weight-bold">${data.Name}</h6><button class="btn btn-primary w-100" data-val-id="${data.Id}">Ver detalles</button> <p class="card-text d-flex align-items-center bd-highlight mb-1"> <span> <img class="w-25" src="img/icon/Servings.png"> ${data.Servings} servings</span> <span class="ml-auto p-2 bd-highlight"> <img class="w-25" src="img/icon/time.png"> ${data.AmountOfTime} min. </span></p> </div> <div class="card-footer bg-dark"> <p class="card-text d-flex align-items-center bd-highlight mb-1">  <img src="img/icon/heart.png" class="mr-2 like-recipes" title="Inicia Sesión para Likear" id="btn-like" data-val-id-likes="0"><span class="text-white h2 m-0">${data.Votes} </span> <small class="text-muted ml-auto p-0 mb-0  bd-highlight h5">By ${data.Author}</small> </p> </div> </div> </article>`);
                    });
                },
                error: function (err) {
                    console.error(`Error al obtener los datos ${err}`);
                }

            });

        }

    }
});

// Fin del codigo que me permitio realizar una busqueda de las recetas de la API.


// Codigo del boton de cerrar cards
function closeCard() {
    location.href = "index.html"
}



// Inicio de codigo al darle click  en el boton agregar mas ingredientes a la receta.
var contador = 1;
var objRecipeIngredientDetails = {};
var arrayRecipeIngredientDetails = [];

function plusData() {
    let ingrediente = $(`#r-ingrediente-${contador - 1}`).val();
    let descripcion = $(`#r-descripcion-${contador - 1}`).val();
    objRecipeIngredientDetails = {
        IngredientName: ingrediente,
        QuantityDescription: descripcion
    };
    arrayRecipeIngredientDetails.push(objRecipeIngredientDetails);
    $('#tbody-add-recipes').append(`<tr><td> <div> <input type="text" class="form-control m-2 " id="r-ingrediente-${contador}" placeholder="Ingresar ingrediente" style="width: 97%;" maxlength="30" required> </div> </td> <td> <div> <input type="text" class="form-control m-2 " id="r-descripcion-${contador}" placeholder="Ingresar descripción" style="width: 97%;" maxlength="20" required> </div> </td> <td> <div id=""> <button class="btn btn-primary rounded-circle mt-2" id="r-button-plus-${contador}" style="width: 44%;" onclick="plusData();">+</button> </div><div id="btn-r-success"> <button class="btn btn-success rounded-circle mt-2" id="r-button-plus-0" style="width: 44%;" onclick="r_Success();"><img src="img/icon/icon-for-register-recipes/checked.png" alt=""> </button> </div> </td></tr>`);
    $(`#r-button-plus-${contador - 1}`).remove();
    $(`#btn-r-success`).remove();
    $(`#r-ingrediente-${contador}`).focus();
    contador++;
};

function r_Success() {
    let ingrediente = $(`#r-ingrediente-${contador - 1}`).val();
    let descripcion = $(`#r-descripcion-${contador - 1}`).val();
    objRecipeIngredientDetails = {
        IngredientName: ingrediente,
        QuantityDescription: descripcion
    };
    arrayRecipeIngredientDetails.push(objRecipeIngredientDetails);
    $('#tabla').html('<div class="alert alert-success p-5" role="alert" id="seccess">Successfully Saved!</div>');
}

// Codigo para mostrar el la text box para la imagen.
function r_imagen_update() {
    $('#r-textbox-update').append('<div class="input-group mb-2" id="textbox-ocultar"><div class="input-group-prepend"><div class="input-group-text bg-white" style="cursor:pointer;" onclick="update_image();"><img src="img/icon/icon-for-register-recipes/upload.png" style="width: 60%;"></div></div><input type="text" class="form-control " id="r-update" placeholder="Pega el enlace de la imagen desde internet"></div>');
    $('#r-update').focus();
}

// Codigo js para eliminar el textbox creado.
function update_image() {
    window.ImageUrl = $('#r-update').val();

    $('#r-image-for-udate-user').html(`<img src="${ImageUrl}"  class="w-100">`);
    $('#textbox-ocultar').remove();

}

// Funcion para enviar todos los datos de la receta al API.
function r_SuccessData() {
    let Name = $('#r-recipes-titulo').val();
    let Description = $('#r-recipes-descripcion').val();
    let Servings = $('#r-cant-platos').val();
    let AmountOfTime = $('#r-recipes-time').val();
    let token = localStorage.getItem('token');
    // El arrays 
    let Steps = $('#r-recipes-step-descripcion').val();
    objRecipesUser = {
        Name: Name,
        Description: Description,
        Servings: Servings,
        AmountOfTime: AmountOfTime,
        RecipeIngredientDetails: arrayRecipeIngredientDetails,
        Steps: Steps,
        ImageUrl: ImageUrl,
        Votes: 0
    };

    $.ajax({
        url: `http://stackoverflowcgvm.apphb.com/api/Recipes`,
        type: 'POST',
        data: objRecipesUser,
        beforeSend: function (dataToken) {
            dataToken.setRequestHeader("Authorization", `bearer ${token}`);
        },
        success: function (data) {
            alert("Exitoso");
            location.href = 'index.html';
        },
        error: function (error) {
            alert("Paso un error: " + error.responseJSON.responseText);
            debugger
        },
    });

}

//Codigo que envia un like al API de la receta que se le da me gusta.

$(document).on("click", "#btn-like", function () {
    let idLikeRecipes = $(this).data("val-id-likes");
    if (idLikeRecipes != 0) {
        let token = localStorage.getItem("token");
        $.ajax({
            url: `http://stackoverflowcgvm.apphb.com/api/Recipes/${idLikeRecipes}`,
            type: 'PUT',
            beforeSend: function (dataToken) {
                dataToken.setRequestHeader("Authorization", `bearer ${token}`);
            },
            success: function (data) {
                location.href = 'index.html';
            },
            error: function (error) {
                alert("Paso un error al darle like: o no le puede dar like por falta de pribilegios" + error.responseJSON.responseText);
            },
        });
    } else {
        location.href = "login.html";
    }

});


//Cargar tarjeta:
