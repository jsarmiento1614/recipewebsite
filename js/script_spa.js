// Declarando las variables para el uso de Single page aplication de app recipes.

// Variables de restringinciones para al usuario.
var userIsLogin;
if(localStorage.token===null || localStorage.token==="undefined"){
    window.islogin=false;
}else{
    window.islogin=true;
}

if(!islogin){
    userIsLogin='<a href="" onclick="javascipt:return false;" id="login"> <p class="d-inline text-white ml-2" title="Inicia sesión o registrate">Iniciar Sesión</p> </a> </a>';
    userAddRecipes='<a class="nav-link" href="login.html" title="Loguearse">Agregar Receta</a>';
}else{
    userIsLogin='<a href="" onclick="javascipt:return false;" id="logout"> <p class="d-inline text-white ml-2">Cerrar Sesión</p> </a> </a>';
    userAddRecipes='<a class="nav-link" href="addRecipes.html">Agregar Receta</a>';
}


// SPA Home for recipesHOME FOR RECIPES
var htmlHomeRecipes=`<nav class="navbar navbar-expand-xl navbar-dark bg-dark text-center position-fixed w-100 mystyle-nav"> <a href="index.html" class="navbar-brand"> <img src="img/logo-Recetas.png" alt="Logo Coorporativo" class="w-50"> </a> <div class="collapse navbar-collapse " id="navbarsExample06"> <ul class="navbar-nav"> <li class="nav-item active"> <a class="nav-link" href="index.html">Inicio </a> </li> <li class="nav-item"> <a class="nav-link" href="#">Nosotros</a> </li> <li class="nav-item"> ${userAddRecipes} </li> <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorias</a> <div class="dropdown-menu" aria-labelledby="dropdown06"> <a class="dropdown-item" href="#">Postres</a> <a class="dropdown-item" href="#">Carnes</a> <a class="dropdown-item" href="#">Ensaladas</a> </div> </li> </ul> <form onsubmit="event.preventDefault();" class="form-inline my-2 my-md-0 w-50"> <input class="form-control w-100" type="text" placeholder="Search" id="search" title="Busca por nombre | author"> </form> <div> <div class="d-inline w-100"> <img src="img/icon/man.png" alt="AvatarUser" class="ml-4"> <a href="#" class=""> ${userIsLogin} </div> <!-- Aqui codigo para registrar Usuario --> </div> </div> </nav> <header class="container-all bg-white"> <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"> <ol class="carousel-indicators"> <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li> <li data-target="#carouselExampleIndicators" data-slide-to="1"></li> <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> </ol> <div class="carousel-inner"> <div class="carousel-item active"> <img class="d-block w-100" src="img/399.jpg" alt="First slide"> <div class="carousel-caption d-none d-md-block"> <h2>LAS MEJORES RECETAS DE MOLE, QUE PUEDES PROBAR</h2> <p>Encuentra recetas fáciles y deliciosas de los mejores moles del país.</p> </div> </div> <div class="carousel-item"> <img class="d-block w-100" src="img/400.jpg" alt="Second slide"> <div class="carousel-caption d-none d-md-block"> <h2>LAS MEJORES RECETAS DE MOLE, QUE PUEDES PROBAR</h2> <p>Encuentra recetas fáciles y deliciosas de los mejores moles del país.</p> </div> </div> <div class="carousel-item"> <img class="d-block w-100" src="img/403.jpg" alt="Third slide"> <div class="carousel-caption d-none d-md-block"> <h2>LAS MEJORES RECETAS DE MOLE, QUE PUEDES PROBAR</h2> <p>Encuentra recetas fáciles y deliciosas de los mejores moles del país.</p> </div> </div> </div> <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div> </header> <section> <div class="row p-3" id="cards"> </div> </section>`;

function register_required(){
    let htmlAlertRegister='<div class="alert alert-warning" role="alert">Para ejecutar funciones de usuario logueado, debes de registrarte! <button class="btn btn-primary">Registrarme</butotn></div>'
    $('cards').append(htmlAlertRegister);
}

loadPage();
// Codigo muestra el login
$(document).on("click", "#login", function login() {
    location.href="../login.html"
});

// Codigo muestra el login
$(document).on("click", "#logout", function logout() {
    alert('Cerrando tu Sesion');
    localStorage.setItem("token", undefined);
    location.href="../index.html"
});

function loadPage(){
    $('#containerHome').html(htmlHomeRecipes);
}
